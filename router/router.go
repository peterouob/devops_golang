package router

import (
	"github.com/gin-gonic/gin"
	"github.com/peterouob/devops_golang/services"
)

func HandleRouter() *gin.Engine {
	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.GET("/", services.GetAll)
	r.GET("/:id", services.GetByID)
	r.POST("/create", services.Create)
	r.PUT("/update/:id", services.Update)
	r.DELETE("/delete/:id", services.Delete)
	return r
}
